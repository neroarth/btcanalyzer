import * as functions from 'firebase-functions';
import * as admin from 'firebase-admin';
import * as dateFormat from 'dateformat';
admin.initializeApp();

const COL_BTC_TRADE_PRICE = 'btcTradePrice';
const COL_TODAY_DATA = 'todayData';

const db = admin.firestore();

// // Start writing Firebase Functions
// // https://firebase.google.com/docs/functions/typescript
//
// export const helloWorld = functions.https.onRequest((request, response) => {
//     response.send("Hello from Firebase!");
//     console.log('This will be run every 1 minutes!1111');
// });

exports.scheduledFunction = functions.pubsub.schedule('* * * * *')
    .timeZone('Asia/Kuala_Lumpur') // Users can choose timezone - default is America/Los_Angeles
    .onRun((context) => {
        console.log('This will be run every minute');

        const currentDateObj = new Date().toLocaleString("en-US", {timeZone: "Asia/Kuala_Lumpur"});
        const currentDate : string = dateFormat(currentDateObj, "yyyymmdd" );
        const currentTime : string = dateFormat(currentDateObj, "HHMMss" );

        console.log(currentDate);
        console.log(currentTime);
        
        return db.collection(COL_BTC_TRADE_PRICE)
            .doc(currentDate)
            .collection(COL_TODAY_DATA)
            .doc(currentTime)
            .set({ 
                myr: 888,
                prevMyr: 999,
                usd: 888,
                prevUsd: 999,
                time: admin.firestore.Timestamp.now()
            })
            .then( (doc: object) => {
                console.log('added successfully');
                return;
            }).catch( (err: object) => {
                console.error(err);
                return;
            });
});
